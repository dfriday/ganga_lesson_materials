#!/usr/bin/env ganga
import argparse

#parser
parser = argparse.ArgumentParser(description="prepare Ganga")

parser.add_argument('arg', 
		    help='some arg')

parser.add_argument('name', 
		    help='some name')

args = parser.parse_args()

arg = args.arg
name = args.name

j = Job(name = f"{name}")
j.application = Executable()
j.application.exe = File("test.sh")
j.inputfiles = [LocalFile("test.py")] 
j.outputfiles = [LocalFile("*.txt")]
j.backend = Dirac()
j.backend.diracOpts = '[j.setTag(["/cvmfs/lhcbdev.cern.ch/"])]'

j.splitter = GenericSplitter()
j.splitter.attribute = 'application.args'
j.splitter.values = [[f'{arg}_0'], [f'{arg}_1'], [f'{arg}_2']]

j.submit()

