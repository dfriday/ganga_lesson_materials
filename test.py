import argparse

parser = argparse.ArgumentParser(description='testing ganga!')

parser.add_argument('test_arg', type=str ,nargs=1,
                    help='any string or number, your choice!')

args = parser.parse_args()

test_arg = args.test_arg[0]

print(f"So we can parse arguments pretty easily this way --> {test_arg}" )
