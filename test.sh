#!/bin/bash

#source LbEnv
source /cvmfs/lhcb.cern.ch/lib/LbEnv --quiet

#run the test script!
lb-conda default python test.py $1
